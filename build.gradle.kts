plugins {
    `java-library`
    `maven-publish`
    id("net.nemerosa.versioning")
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    api(libs.net.java.dev.jna.jna.jpms)
    api(libs.net.java.dev.jna.jna.platform.jpms)
}

group = "uk.co.caprica"
version = versioning.info.tag ?: versioning.info.full
description = "vlcj-natives"

val testsJar by tasks.registering(Jar::class) {
    archiveClassifier.set("tests")
    from(sourceSets["test"].output)
}

java {
    withSourcesJar()
    withJavadocJar()

    toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
